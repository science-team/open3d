#!/usr/bin/make -f
export DEB_BUILD_MAINT_OPTIONS = hardening=+all reproducible=+all future=+lfs
export PYBUILD_NAME=open3d

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

# -faligned-new is needed on some architectures to support 16 byte alignment
# of Eigen types
# -Wno-psabi silences the note about an ABI change in GCC 7.1, which comes
# from STL code
export DEB_CFLAGS_MAINT_APPEND = -faligned-new -Wno-psabi
export DEB_CXXFLAGS_MAINT_APPEND = -faligned-new -Wno-psabi
export SPHINX_TODAY = $(shell LC_ALL=C date -u "+%B %d, %Y" -d "@$(SOURCE_DATE_EPOCH)")

WITH_EMBREE = $(if $(filter amd64 arm64,$(DEB_HOST_ARCH)),ON,OFF)
BUILD_UNIT_TESTS = OFF  # $(if $(filter nocheck,$(DEB_BUILD_OPTIONS)),OFF,ON)
BUILD_CUDA = OFF
BUILD_PYTORCH_OPS = OFF  # $(if $(filter amd64 arm64 ppc64el riscv64 s390x,$(DEB_HOST_ARCH)),ON,OFF)

BUILD_DIR = obj-$(DEB_HOST_GNU_TYPE)
LIB_DIR = $(CURDIR)/$(BUILD_DIR)/lib/None

%:
	dh $@ --buildsystem=cmake

override_dh_auto_configure:
	dh_auto_configure --buildsystem=cmake -- \
		-DCMAKE_SKIP_RPATH=ON \
		-DDEVELOPER_BUILD=OFF \
		-DGLIBCXX_USE_CXX11_ABI=1 \
		-DBUILD_SHARED_LIBS=ON \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_CUDA_MODULE=$(BUILD_CUDA) \
		-DBUILD_COMMON_CUDA_ARCHS=$(BUILD_CUDA) \
		-DCUDAToolkit_INCLUDE_DIR=/usr/include \
		-DBUILD_UNIT_TESTS=$(BUILD_UNIT_TESTS) \
		-DBUILD_ISPC_MODULE=OFF \
		-DBUILD_WEBRTC=OFF \
		-DBUILD_PYTORCH_OPS=$(BUILD_PYTORCH_OPS) \
		-DUSE_BLAS=ON \
		-DWITH_IPP=OFF \
		-DUSE_SYSTEM_ASSIMP=ON \
		-DUSE_SYSTEM_BLAS=ON \
		-DUSE_SYSTEM_CURL=ON \
		-DUSE_SYSTEM_CUTLASS=ON \
		-DUSE_SYSTEM_EIGEN3=ON \
		-DUSE_SYSTEM_EMBREE=ON \
		-DUSE_SYSTEM_FAISS=ON \
		-DUSE_SYSTEM_FILAMENT=ON \
		-DUSE_SYSTEM_FLANN=ON \
		-DUSE_SYSTEM_FMT=ON \
		-DUSE_SYSTEM_GLEW=ON \
		-DUSE_SYSTEM_GLFW=ON \
		-DUSE_SYSTEM_GOOGLETEST=ON \
		-DUSE_SYSTEM_IMGUI=ON \
		-DUSE_SYSTEM_JPEG=ON \
		-DUSE_SYSTEM_JSONCPP=ON \
		-DUSE_SYSTEM_LIBLZF=ON \
		-DUSE_SYSTEM_MSGPACK=ON \
		-DUSE_SYSTEM_NANOFLANN=ON \
		-DUSE_SYSTEM_OPENSSL=ON \
		-DUSE_SYSTEM_PNG=ON \
		-DUSE_SYSTEM_PYBIND11=ON \
		-DUSE_SYSTEM_QHULLCPP=ON \
		-DUSE_SYSTEM_STDGPU=ON \
		-DUSE_SYSTEM_TBB=ON \
		-DUSE_SYSTEM_TINYGLTF=ON \
		-DUSE_SYSTEM_TINYOBJLOADER=ON \
		-DUSE_SYSTEM_VTK=ON \
		-DUSE_SYSTEM_ZEROMQ=ON \
		-DWITH_EMBREE=$(WITH_EMBREE) \
		-DWITH_MINIZIP=ON

override_dh_auto_test:
ifeq ($(BUILD_UNIT_TESTS),ON)
	LD_LIBRARY_PATH=$(LIB_DIR) OPEN3D_TORCH_OP_LIB=$(LIB_DIR)/cpu/open3d_torch_ops.so \
	./$(BUILD_DIR)/bin/tests || true
endif

execute_after_dh_auto_clean:
	find . -name __pycache__ -type d -print0 | xargs -r0 rm -r

execute_after_dh_auto_install:
	# Build Python package
	$(MAKE) -C $(BUILD_DIR) python-package
	cd $(BUILD_DIR)/lib/python_package && python3 setup.py install --install-layout=deb --root=$(CURDIR)/debian/python3-open3d
	rm -r debian/python3-open3d/usr/lib/python*/*-packages/open3d/resources
	# Fix GUI application installation
	mv debian/tmp/usr/bin/Open3D debian/tmp
	mv debian/tmp/Open3D/Open3D debian/tmp/usr/bin
	rm debian/tmp/Open3D/resources/Roboto-*
	mkdir -p debian/tmp/usr/share/open3d-viewer
	mv debian/tmp/Open3D/resources debian/tmp/usr/share/open3d-viewer
	rm -r debian/tmp/Open3D
	rm -r debian/tmp/usr/share/resources

execute_after_dh_install-indep:
ifeq ($(BUILD_PYTORCH_OPS),ON)
	rm -r debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libopen3d$(OPEN3D_SOVERSION)
endif

execute_after_dh_install:
ifeq ($(BUILD_PYTORCH_OPS),ON)
	# Move ops library to destination or delete it for indep builds
	if test -d debian/libopen3d$(OPEN3D_SOVERSION)/usr/lib/$(DEB_HOST_MULTIARCH); then \
		mv debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libopen3d$(OPEN3D_SOVERSION) debian/libopen3d$(OPEN3D_SOVERSION)/usr/lib/$(DEB_HOST_MULTIARCH); \
	else \
		rm -r debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libopen3d$(OPEN3D_SOVERSION); \
	fi
endif

PYTHON_DOC_DIR = debian/open3d-doc/usr/share/doc/open3d-doc/python
DOXYGEN_DOC_DIR = debian/open3d-doc/usr/share/doc/open3d-doc/cpp
MAKE_DOCS_COMMON_OPTS = --clean_notebooks --execute_notebooks=never --is_release

execute_before_dh_installdocs-indep:
ifeq ($(filter nodoc,$(DEB_BUILD_OPTIONS)),)
	pandoc -f markdown_strict -t rst -o README.rst README.md
	# Build C++ (Doxygen) documentation
	rm -rf docs/_out
	cd docs && LC_ALL=C LD_LIBRARY_PATH=$(LIB_DIR) OPEN3D_TORCH_OP_LIB=$(LIB_DIR)/cpu/open3d_torch_ops.so PYTHONPATH=$(shell ls -d $(CURDIR)/debian/python3-open3d/usr/lib/python*/*-packages) \
		python3 make_docs.py $(MAKE_DOCS_COMMON_OPTS) --doxygen --py_api_rst=never --py_example_rst=never
	mkdir -p $(DOXYGEN_DOC_DIR)
	cp -r docs/_out/html/cpp_api $(DOXYGEN_DOC_DIR)/html
	dh_doxygen
	# Build Python (Sphinx) documentation
	rm -rf docs/_out
	cd docs && LC_ALL=C LD_LIBRARY_PATH=$(LIB_DIR) OPEN3D_TORCH_OP_LIB=$(LIB_DIR)/cpu/open3d_torch_ops.so PYTHONPATH=$(shell ls -d $(CURDIR)/debian/python3-open3d/usr/lib/python*/*-packages) \
		python3 make_docs.py $(MAKE_DOCS_COMMON_OPTS) --sphinx
	mkdir -p $(PYTHON_DOC_DIR)
	cp -r docs/_out/html $(PYTHON_DOC_DIR)
endif

execute_after_dh_installexamples-indep:
	# Remove erroneous executable bit from some documentation files
	find debian/open3d-doc/usr/share/doc/open3d-doc/examples -type f -print0 | xargs -r -0 chmod -x
